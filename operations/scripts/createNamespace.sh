#!/bin/bash

KUBE_CONTEXT=${1}

if [ -n "${KUBE_CONTEXT}" ]; then

  kubectl --context ${KUBE_CONTEXT} create namespace express-dev
  kubectl --context ${KUBE_CONTEXT} create serviceaccount --namespace express-dev express-dev-sa
  kubectl --context ${KUBE_CONTEXT} create rolebinding kops-service-binding --clusterrole=admin --serviceaccount=express-dev:express-dev-sa --namespace=express-dev
  kubectl --context ${KUBE_CONTEXT} create serviceaccount tiller --namespace express-dev
  kubectl --context ${KUBE_CONTEXT} create rolebinding tiller-binding --clusterrole=admin --serviceaccount=express-dev:tiller --namespace=express-dev
  helm init --kube-context ${KUBE_CONTEXT} --service-account tiller --tiller-namespace express-dev
  kubectl --context ${KUBE_CONTEXT} patch deploy --namespace express-dev tiller-deploy -p '{"spec":{"template":{"spec":{"automountServiceAccountToken":true}}}}'

  ./create-kubeconfig.sh express-dev-sa --context ${KUBE_CONTEXT} --namespace express-dev > config/${KUBE_CONTEXT}-kubeconfig.yaml

else

  kubectl create namespace express-dev
  kubectl create serviceaccount --namespace express-dev express-dev-sa
  kubectl create rolebinding kops-service-binding --clusterrole=admin --serviceaccount=express-dev:express-dev-sa --namespace=express-dev
  kubectl create serviceaccount tiller --namespace express-dev
  kubectl create rolebinding tiller-binding --clusterrole=admin --serviceaccount=express-dev:tiller --namespace=express-dev
  helm init --service-account tiller --tiller-namespace express-dev
  kubectl patch deploy --namespace express-dev tiller-deploy -p '{"spec":{"template":{"spec":{"automountServiceAccountToken":true}}}}'

  ./create-kubeconfig.sh express-dev-sa --namespace express-dev > config/kubeconfig.yaml

fi