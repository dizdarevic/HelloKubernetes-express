FROM node:8.11.3-alpine
MAINTAINER Brice Rising

# Add project files
ADD . HelloKubernetes-express
WORKDIR HelloKubernetes-express

# Install dependencies
RUN npm install

EXPOSE 8080
ENTRYPOINT [ "npm", "start" ]
