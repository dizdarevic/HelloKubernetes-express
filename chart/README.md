Settable values:

    helloKubernetes.express.deployment.image
        - String - Used to override the default image that is deployed
        - default - docker.io/bricerisingslalom/hello-kubernetes-express
    helloKubernetes.express.ingress.path
        - String - Used to override the path for ingress rule that is created
        - default - /express
    helloKubernetes.express.service.annotations 
        - Map - Used to pass annotations to the service
        - default - {}
    helloKubernetes.express.service.type
        - String - Used to override the default service type
        - default - ClusterIP
    helloKubernetes.express.service.loadBalancerSourceRanges
        - String - Used to override the ip range that has access to your server (important when using a load balancer)
        - default - 0.0.0.0/0
