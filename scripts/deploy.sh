if [ -z `helm list | grep hello-kubernetes-express` ]; then
    helm --name hello-kubernetes-express install chart
else
    helm upgrade hello-kubernetes-express chart --recreate-pods
fi
